package projet.model;

import java.io.Serializable;
import java.sql.Date;
import java.util.ArrayList;
import java.util.Random;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Compte implements Serializable{
	private static final long serialVersionUID = -8537962680206576813L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	//enssemble de variable a l'inscription
	private Long id;
	private String pseudo;
	private String email;
	private Date naissance;
	private String mdp;
	//
	private Integer token;
	public Compte() {
		// TODO Auto-generated constructor stub
	}
	//information compte
	public Compte(String pseudo, String email, Date naissance,String mdp) {
		this.pseudo = pseudo;
		this.email = email;
		this.naissance = naissance;
		this.mdp = mdp;
		token = 0;
	}
	//generation du token
	public void RandomToken() {
		Random r =new Random();
		token = r.nextInt();
	}
	//fonction permetant d'acceder aux données de la classe
	public Integer getToken() {
		return token;
	}
	public void setToken(Integer token) {
		this.token = token;
	}
	public String getPseudo() {
		return pseudo;
	}
	public void setPseudo(String pseudo) {
		this.pseudo = pseudo;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public Date getNaissance() {
		return naissance;
	}
	public void setNaissance(Date naissance) {
		this.naissance = naissance;
	}
	public String getMdp() {
		return mdp;
	}
	public void setMdp(String mdp) {
		this.mdp = mdp;
	}
	
	
}

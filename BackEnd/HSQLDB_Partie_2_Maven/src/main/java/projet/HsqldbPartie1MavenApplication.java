package projet;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HsqldbPartie1MavenApplication {
	/*
	 * Permet de lancer le serveur
	 */
	public static void main(String[] args) {
		SpringApplication.run(HsqldbPartie1MavenApplication.class, args);
	}

}

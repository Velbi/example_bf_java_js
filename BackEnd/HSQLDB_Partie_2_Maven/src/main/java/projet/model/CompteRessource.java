package projet.model;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.springframework.beans.factory.annotation.Autowired;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;
import projet.repository.CompteRepository;

/*
 * fichier qui reference tout les url pour accéde au information d'un compte 
 */
@Path("/Compte")
public class CompteRessource {
	@Autowired
	private CompteRepository compterepository;
	final static Logger logger = LogManager.getLogger(CompteRessource.class);
	
	public CompteRessource() {}
	
	//Création d'un nouveau compte dans la base
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/cmpt/create")
	public Compte createCompte(Compte c) {
		logger.info("This is info Création du compte: " + c.getPseudo());
		return compterepository.save(c);		
	}
	
	//Retourne tout les compte de la base
	@GET
	@Produces("application/json")
	@Path("/cmpt/readAll")
	public List<Compte> readAll(){
		return (List<Compte>) compterepository.findAll();
	}
	
	//Retourne un compte par son pseudo
	@GET
	@Produces("application/json")
	@Path("/cmpt/searchCompteByPseudo")
	public List<Compte> readCompteByPseudo(@QueryParam("pseudo") String pseudo) {
		return (List<Compte>) compterepository.findByPseudo(pseudo);
	}
	
	//Retourne un compte par son token
	@GET
	@Produces("application/json")
	@Path("/cmpt/searchCompteByToken")
	public Compte readCompteByToken(@QueryParam("token") Integer token) {
		return compterepository.findByToken(token);
	}
	
	//Met a jour le compte
	public boolean updateCompte() {
		return false;
	}
	
	//Suprime un compte
	public boolean deleteCompte() {
		return false;
	}
	/*
	 * permet de se connecter a son compte et avoir un token
	 */
	@GET
	@Produces("application/json")
	@Path("/cmpt/connectionCompte")
	public Compte Connection_Compte(@QueryParam("email") String email,@QueryParam("mdp") String mdp) {
		for (Compte element : compterepository.findByEmail(email)) {
			if(mdp.equals(element.getMdp())) {
				element.RandomToken();
				compterepository.save(element);
				return element;
			}
		}
		return null;
	}
}

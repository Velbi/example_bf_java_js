//js de la page connection
// grace a ce fichier je peux me connecté a mon compte 
$(document).ready(function () {

		$("#connection").click(function () {
			var email = $("#email").val();
			var mdp = $("#mdp").val();
			$.get("http://localhost:8080/rest/Compte/cmpt/connectionCompte",{email:email,mdp:mdp}, function (resp) {
				if (resp.mdp== mdp) {
					//sauvegarde le token dans une session pour le recup dans une autre page
					sessionStorage.setItem("token", resp.token);
					//renvoie sur la page compte apres la connection
					window.location = "./Compte.html";
				}
			});
		});
	});
	

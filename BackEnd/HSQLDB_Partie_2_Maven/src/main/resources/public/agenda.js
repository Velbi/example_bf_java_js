/*
 * fichier qui permet d'avoir les evenements d'un compte
 */
const getUrl = (path = "/") => {
		const protocol = "http";
		const address = "localhost";
		const port = 8080;
		return protocol + '://' + address + ':' + port + '' + path;
	};

	$(document).ready(function () {
		// recuperation du token 
		var token = sessionStorage.getItem("token");
		// le chef est l'organisateur de l'evenement 
		var chef;
		// grace au token sauvegarder dans la sesion je peux recuperer le compte
		$.get("http://localhost:8080/rest/Compte/cmpt/searchCompteByToken",{token:token}, function (resp) {
			chef = resp.pseudo ;	
		});
		// affiche tout les evenements du compte actif
		$.get("http://localhost:8080/rest/Evenement/eve/readAll", function (resp) {
			$.each(resp, function (i, item) {
				if(chef == item.chef){
					$("#All_Even").append('<tr><td style="border: thin  solid #000000;" >' + item.chef + '</td><td style="border: thin  solid #000000;" >' + item.name + '</td><td style="border: thin  solid #000000;" >' + item.description + '</td><td style="border: thin  solid #000000;" >' + item.debut + '</td><td style="border: thin  solid #000000;" >' + item.fin + '</td></tr>')
				}
			});
		});
		// affiche une recherche d'un evenement par son titre
		$("#search_titre").click(function(){
			var name = $("#Sname").val();
			$.get("http://localhost:8080/rest/Evenement/eve/searchEvenementByName",{name:name}, function (resp) {
				$.each(resp, function (i, item) {
					if(chef == item.chef){
					$("#res_search_titre").append('<li>' + item.name + ' ' + item.description + ' ' + item.debut + ' ' + item.fin + '</li>')
					}
				});
			});
		});	
		$("#search_debut").click(function(){
			var debut = $("#Sdebut").val();
			$.get("http://localhost:8080/rest/Evenement/eve/searchEvenementByDebut",{debut:debut}, function (resp) {
				$.each(resp, function (i, item) {
					if(chef == item.chef){
					$("#res_search_debut").append('<li>' + item.name + ' ' + item.description + ' ' + item.debut + ' ' + item.fin + '</li>')
				}
				});
			});
		});
		$("#search_fin").click(function(){
			var fin = $("#Sfin").val();
			$.get("http://localhost:8080/rest/Evenement/eve/searchEvenementByFin",{fin:fin}, function (resp) {
				$.each(resp, function (i, item) {
					if(chef == item.chef){
					$("#res_search_fin").append('<li>' + item.name + ' ' + item.description + ' ' + item.debut + ' ' + item.fin + '</li>')
				}
				});
			});
		});
		// fonction permettant la suppression d'un evenement par son titre seul fonction qui utilise le @PathParam
		$("#delete_titre").click(function(){
			var name = $("#Dname").val();
			$.get("http://localhost:8080/rest/Evenement/eve/searchEvenementByName",{name:name}, function (resp) {
				$.each(resp, function (i, item) {
					if(chef == item.chef){
						$.ajax({
				    		url: "http://localhost:8080/rest/Evenement/eve/deleteEvenementByName/"+name+"",
				    		type: "DELETE",
				    		
				    		success: function(result) {
				    	    	location.reload() ;
				    		},
				    		error: function(request,msg,error) {
				    	    
				    		}
						});
					}
				});
			});
			
		});
		/*
		*création d'un compte 
		*/
		$("#creation").click(function () {
			var name = $("#name").val();
			var description = $("#description").val();
			var debut = $("#debut").val();
			var fin = $("#fin").val();
			$.ajax({
				type: "post",
				url: getUrl("/rest/Evenement/eve/create"),
				contentType: "application/json; charset=UTF-8",
				data: JSON.stringify({
					chef:chef,
					name:name,
					description:description,
					debut:debut,
					fin:fin
				}),
				dataType: "json",
				success: function (result) {
					location.reload() ;
				}
			});
		});	
	});  	

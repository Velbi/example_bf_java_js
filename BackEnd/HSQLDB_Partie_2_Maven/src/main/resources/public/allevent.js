
/*
 * Fichier permettant de voire tout les evenements et faire une recherche par titre
 */
	$(document).ready(function () {
		$.get("http://localhost:8080/rest/Evenement/eve/readAll", function (resp) {
			$.each(resp, function (i, item) { //parcours tout les responses
				$("#All_Even").append('<tr><td style="border: thin  solid #000000;" >' + item.chef + '</td><td style="border: thin  solid #000000;" >' + item.name + '</td><td style="border: thin  solid #000000;" >' + item.description + '</td><td style="border: thin  solid #000000;" >' + item.debut + '</td><td style="border: thin  solid #000000;" >' + item.fin + '</td></tr>')
			});
		});
		
		$("#search_titre").click(function(){
			var name = $("#Sname").val();
			$.get("http://localhost:8080/rest/Evenement/eve/searchEvenementByName",{name:name}, function (resp) {
				$.each(resp, function (i, item) { //parcours tout les responses
					$("#res_search_titre").append('<li>' + item.name + ' ' + item.description + ' ' + item.debut + ' ' + item.fin + '</li>')
				});
			});
		});
		$("#search_debut").click(function(){
			var debut = $("#Sdebut").val();
			$.get("http://localhost:8080/rest/Evenement/eve/searchEvenementByDebut",{debut:debut}, function (resp) {
				$.each(resp, function (i, item) { //parcours tout les responses
					$("#res_search_debut").append('<li>' + item.name + ' ' + item.description + ' ' + item.debut + ' ' + item.fin + '</li>')
				});
			});
		});
		$("#search_fin").click(function(){
			var fin = $("#Sfin").val();
			$.get("http://localhost:8080/rest/Evenement/eve/searchEvenementByFin",{fin:fin}, function (resp) {
				$.each(resp, function (i, item) { //parcours tout les responses
					$("#res_search_fin").append('<li>' + item.name + ' ' + item.description + ' ' + item.debut + ' ' + item.fin + '</li>')
				});
			});
		});
		
		
	});  	

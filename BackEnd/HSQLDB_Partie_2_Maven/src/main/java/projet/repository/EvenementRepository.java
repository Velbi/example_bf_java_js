package projet.repository;

import java.util.Date;
import java.util.List;


import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

import projet.model.Evenement;


public interface EvenementRepository  extends CrudRepository<Evenement, Long>{
	List<Evenement> findByName(String name);
	List<Evenement> findByFin(Date debut);
	List<Evenement> findByDebut(Date fin);
	@Transactional
	List<Evenement> deleteByName(String name);
}
